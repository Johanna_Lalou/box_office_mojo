"""Configuration file with all the details regarding the inputs of the python file scraping.py"""

import datetime
from scraping import get_all_columns, get_all_years

"""To modify the configurations, the changes must be made just below.
There are 6 required inputs and 1 optional input.
1st element: 
2 Possibilities for this input.
possibility 1: a start Year like 2017
possibility 2: a Start Date that must be inputted in the format of a list as [Year,Month,Day].

2nd input:
The same as above but an ending year or ending date.

3rd input: 
A list of the column names we want from the table. for example ['Date (clickto view)', 'Day', '#1 Movie', 'Gross']
If you would like all the columns, simply input [].

4th input (Optional):
input a list of the days of the week you would like in the Dataframe, such as ['Mon','Tue']
If you omit this input, then all the days of the week will be returned.

5th input :
Your username for SQL

6th input:
Your password for SQL

7th input (immutable):
The name of your database"""

start_date = 2018
end_date = 2019
columns = []
args =  ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
username = 'ubuntu'
pw = 'jojoyoyo88'
db = 'Box_Office_Mojo'
