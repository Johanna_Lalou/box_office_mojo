import pymysql
import config


def create_bom_db():

    connection = pymysql.connect(user=config.username, password=config.pw)
    cur = connection.cursor()

    cur.execute("CREATE DATABASE Box_Office_Mojo;")
    cur.execute("USE Box_Office_Mojo;")
    cur.execute('''CREATE TABLE Movie_Values (
                            Movie_Name VARCHAR(255),
                            Distributor VARCHAR(255),
                            Release_Date DATE,
                            Genre VARCHAR(255),
                            MPAA_Rating VARCHAR(255),
                            Production_Budget DECIMAL,
                            Runtime TIME,
                            Plot VARCHAR(255),
                            Actors VARCHAR(255),
                            imdbRating FLOAT,
                            imdbVotes INT,
                            Country VARCHAR(255),
                            Director VARCHAR(255),
                            Writer VARCHAR(255),
                            PRIMARY KEY(movie_name)
                            );''')
    cur.execute('''CREATE TABLE Daily_Index(
                    Daily_Date DATE,
                    Top_Movie VARCHAR(255) NOT NULL, 
                    Day VARCHAR(255),
                    Top_10_Gross INT,
                    P_change_yd DECIMAL,
                    P_change_lw DECIMAL,
                    Movies_Tracked INT,
                    Gross INT,
                    PRIMARY KEY(daily_date),
                    FOREIGN KEY (Top_Movie) REFERENCES Movie_Values(Movie_Name)
                    );''')
    cur.execute('''SHOW TABLES;''')
    tables = cur.fetchall()

def main():
    create_bom_db()

if __name__ == '__main__':
    main()



# INSERT IGNORE INTO books
#     (id, title, author, year_published)
# VALUES
#     (1, 'Green Eggs and Ham', 'Dr. Seuss', 1960);