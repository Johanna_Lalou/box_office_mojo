"""Authors: Yohan Medalsy and Johanna Lalou
   Data mining project on the Wesbsite BoxOfficeMojo and more specifically the page
   https://www.boxofficemojo.com/daily/?view=year&p=.htm"""

import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import datetime
import config
import pymysql
import urllib
from urllib import error, parse, request
import json

TABLE_NUM = 1
ZERO_INDEX = 0
FIRST_COL_INDEX = 2
NUMBER_MONTHS = 12
NUMBER_DAYS_MONTH  = 31
SPLIT_INDEX = 7
NEW_COLUMNS = 4
FILM_VALUE = 8
DAY_NUMBER_VALUE = 5
DATE_DICT = {"Jan.": 1, "Feb.": 2, "Mar.": 3, "Apr.": 4, "May": 5, "Jun.": 6, "Jul.": 7, "Aug.": 8, "Sept.": 9,
             "Oct.": 10, "Nov.": 11, "Dec.": 12}
SITE_DOMAIN = "https://www.boxofficemojo.com"
REPLACE_DAILY = "main"
NUM_TOTAL_COLUMNS = 10
URL = "https://www.boxofficemojo.com/daily/?view=year&p=.htm"
PARSER = 'html.parser'
DICT_COLUMNS = {'Date (clickto view)': 1, 'Day': 2, 'Top 10 Gross': 4, '% Change YD': 5, '% Change LW': 6,
                'MoviesTracked': 7, '#1 Movie': 8, 'Gross': 9}
KEY_IMDB = '866fe70d'
IMDB_URL = 'http://www.omdbapi.com/?'
APIKEY = '&apikey='+ KEY_IMDB
IMDB_VALUES = ['Plot','Actors','imdbRating','imdbVotes','Country','Director', 'Writer']


def soup_maker(url, parser):
    """This function creates a soup from a url"""
    page = requests.get(url)
    soup = BeautifulSoup(page.content, parser)
    return soup


def get_all_years():
    """This function returns all the years available in the URL, we use it if we want to scrape all the years"""
    year_soup = soup_maker(URL, PARSER)
    df_all_years = year_soup.find_all('table')[TABLE_NUM].p.b.text.split(' - ')[::-TABLE_NUM]
    return df_all_years


def get_all_columns():
    """This function returns all the columns available in the table in the URL,
     we use it if we want to scrape all the columns available """
    title_soup = soup_maker(URL, PARSER)
    df_columns = []
    # We take all the columns except two that we think are useless for any application
    for i in range(FIRST_COL_INDEX + TABLE_NUM, NUM_TOTAL_COLUMNS + TABLE_NUM):
        if i != DAY_NUMBER_VALUE:
            # Here, we deal with the fact that we had a column named % Change YD / LW that we wanted to split
            if i == SPLIT_INDEX:
                split_change = title_soup.find_all('td')[SPLIT_INDEX].find_all('a')
                change_string = title_soup.find_all('td')[SPLIT_INDEX].font
                final_change = change_string.text[ZERO_INDEX:change_string.text.find(split_change[ZERO_INDEX].text)]
                df_columns.append(final_change + " " + split_change[ZERO_INDEX].text)
                df_columns.append(final_change + " " + split_change[TABLE_NUM].text)
            else:
                df_columns.append(title_soup.find_all('td')[i].a.text)
    return df_columns


def config_manipulations(start_date, end_date, columns, *args):
    """ This function takes the elements imported from the config file and makes sure we change our
     dates formats to the datetime format and we get the default elements for each type of configuration if no
      specifications """
    if isinstance(start_date, tuple) or isinstance(start_date, list):
        if len(args) != ZERO_INDEX:
            my_options = {'dates_filter': [to_datetime(start_date[ZERO_INDEX], start_date[TABLE_NUM], start_date[FIRST_COL_INDEX]), to_datetime(
                end_date[ZERO_INDEX], end_date[TABLE_NUM], end_date[FIRST_COL_INDEX])], 'days_filter': args[ZERO_INDEX]}
        else:
            my_options = {'dates_filter': [to_datetime(start_date[ZERO_INDEX], start_date[TABLE_NUM], start_date[FIRST_COL_INDEX]), to_datetime(
                end_date[ZERO_INDEX], end_date[TABLE_NUM], end_date[FIRST_COL_INDEX])],
                          'days_filter': ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']}
        if start_date[ZERO_INDEX] != end_date[ZERO_INDEX]:
            my_years = []
            for my_year in range(int(start_date[ZERO_INDEX]), int(end_date[ZERO_INDEX]) + TABLE_NUM):
                my_years.append(my_year)
        else:
            my_years = [start_date[ZERO_INDEX]]
    else:
        if int(end_date) == datetime.datetime.now().year:
            if len(args) != ZERO_INDEX:
                my_options = {'dates_filter': [to_datetime(start_date, TABLE_NUM, TABLE_NUM), to_datetime(end_date,
                            datetime.datetime.now().month,  datetime.datetime.now().day - TABLE_NUM)], 'days_filter': args[ZERO_INDEX]}
            else:
                my_options = {'dates_filter': [to_datetime(start_date, TABLE_NUM, TABLE_NUM), to_datetime(end_date,
                                         datetime.datetime.now().month, datetime.datetime.now().day - TABLE_NUM)],
                              'days_filter': ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']}
            if start_date != end_date:
                my_years = []
                for my_year in range(int(start_date), int(end_date) + TABLE_NUM):
                    my_years.append(my_year)
            else:
                my_years = [start_date]
        else:
            if len(args) != ZERO_INDEX:
                my_options = {'dates_filter': [to_datetime(start_date, TABLE_NUM, TABLE_NUM), to_datetime(end_date, NUMBER_MONTHS, NUMBER_DAYS_MONTH)],
                              'days_filter': args[ZERO_INDEX]}
            else:
                my_options = {'dates_filter': [to_datetime(start_date, TABLE_NUM, TABLE_NUM), to_datetime(end_date, NUMBER_MONTHS, NUMBER_DAYS_MONTH)],
                              'days_filter': ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']}
            if start_date != end_date:
                my_years = []
                for my_year in range(int(start_date), int(end_date) + TABLE_NUM):
                    my_years.append(my_year)
            else:
                my_years = [start_date]
    return my_years, columns, my_options

def to_datetime(year, month, day):
    """Creates a datetime object with an year, a month and a day """
    return datetime.date(int(year), month,int(day))


def search_movie(title):
    try:
        url = IMDB_URL + urllib.parse.urlencode({'t': title})+ APIKEY
        uh = urllib.request.urlopen(url)
        data = uh.read()
        json_data = json.loads(data)
        return json_data
    except urllib.error.URLError as e:
        print("ERROR: {}".format(e))


def scraper(years=get_all_years(), df_columns=get_all_columns(), **kwargs):
    """Function scraper scraping the data with the options inserted in the config file"""
    my_array = []
    for year in years:
        soup = soup_maker(str(URL.split('&')[ZERO_INDEX] + '&yr={}' + URL.split('&')[ZERO_INDEX]).format(year), PARSER)
        # Here, we get the information gathered in every row of the table in the URL
        soup2 = soup.find_all('tr', attrs={'bgcolor': True})
        for i in range(len(soup2)):
            soup3 = soup2[i].find_all('td')
            liste_values = []
            # We take only the values from the columns we want
            liste_indexes = {key: DICT_COLUMNS[key] for key in df_columns if key in DICT_COLUMNS.keys()}.values()
            for j in liste_indexes:
                if j == TABLE_NUM:
                    # We check if we have options, meaning if we want a range of dates among the years we chose or not
                    if len(kwargs) != ZERO_INDEX and j == DICT_COLUMNS['Date (clickto view)']:
                        # We act accordingly if we have a range of dates (we just add values in our range of dates)
                        if check_dates_filtering(kwargs, year, soup3):
                            liste_values.append(to_datetime(year, DATE_DICT[soup3[j].text.split()[ZERO_INDEX]],
                                             soup3[j].text.split()[TABLE_NUM]).strftime('%m/%d/%Y'))
                        else:
                            break
                    else:
                        liste_values.append(to_datetime(year, DATE_DICT[soup3[j].text.split()[ZERO_INDEX]],
                                             soup3[j].text.split()[TABLE_NUM]).strftime('%m/%d/%Y'))
                # When we arrive to the last column we wanted the scrape, we append interesting data from the movie page
                elif j == sorted(liste_indexes).pop():
                    liste_values = add_extra_data_movies(liste_values, soup3, j)
                    liste_values = add_extra_data_movies_imdb(liste_values, soup3)
                else:
                    # We check if we have options on days, meaning if we want some specific days to be chosen
                    if j == FIRST_COL_INDEX and len(kwargs) != ZERO_INDEX:
                        # We act accordingly if we have a range of days (we just add values in our range of days)
                        if soup3[j].text in kwargs['days_filter']:
                            liste_values.append(soup3[j].text)
                        else:
                            break
                    else:
                        liste_values.append(soup3[j].text)
            if len(liste_values) > TABLE_NUM:
                my_array.append(liste_values)
    # Then, we gather the column names that we added to the dataframe
    df_columns = add_extra_columns(df_columns)
    df_columns = add_extra_columns_imdb(df_columns)
    my_dataframe = pd.DataFrame(np.array(my_array), columns=df_columns)
    return my_dataframe


def conv_dol_to_num(df_col):
    """Function taking off dollars and creating a numerical column"""
    df_col = df_col.str.replace(',', '')
    df_col = df_col.str.replace('$', '')
    df_col = pd.to_numeric(df_col, errors='coerce')
    return df_col


def conv_budjet_to_num(df_col):
    """Function converting the budget column into a numerical column"""
    df_col = df_col.apply(lambda x: x.split()[ZERO_INDEX])
    df_col = df_col.str.replace('$', '')
    df_col = pd.to_numeric(df_col, errors='coerce')
    return df_col


def conv_per_to_num(df_col):
    """Function taking off the percentage in the column"""
    df_col = df_col.str.replace('%', '')
    df_col = pd.to_numeric(df_col, errors='coerce')
    return df_col


def pad_strip(my_string):
    """Function taking off spaces"""
    return '"' + my_string.lstrip() + '"'


def conv_string_date_sql(my_string):
    """Function converting strings to dates"""
    new_string = my_string.lstrip()
    read_date = datetime.datetime.strptime(new_string, '%B %d, %Y')
    return pad_strip(read_date.strftime('%Y-%m-%d'))


def conv_daily_date_sql(my_string):
    """Function dealing with daily dates"""
    new_string = my_string.lstrip()
    read_date = datetime.datetime.strptime(new_string, '%m/%d/%Y')
    return pad_strip(read_date.strftime('%Y-%m-%d'))


def conv_string_hour_sql(my_string):
    """Function dealing with hours wrong format"""
    new_string = my_string.lstrip()
    read_time = datetime.datetime.strptime(new_string, '%H hrs. %M min.')
    return pad_strip(read_time.strftime('%H:%M:%S'))

def remove_quotes(my_string):
    """Function removing the quotes"""
    return pad_strip(my_string.replace('"', ""))


def insert_into_movie_values(my_dataframe):
    """Function inserting values info the table movie_values"""
    connection = pymysql.connect(user=config.username, password=config.pw, database = config.db)
    cur = connection.cursor()

    my_dataframe['Production Budget'] = conv_budjet_to_num(my_dataframe['Production Budget'])
    my_dataframe['Production Budget'] = my_dataframe['Production Budget'].fillna('NULL')

    for my_index, my_row in my_dataframe.iterrows():
        cur.execute("""INSERT IGNORE INTO Movie_Values
                        (Movie_Name, Distributor, Release_Date, Genre, MPAA_Rating, Production_Budget, Runtime, Plot, 
                        Actors, imdbRating, imdbVotes, Country,  Director, Writer)
                        VALUES
                        ({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {});""".format(pad_strip(my_row['#1 Movie']),
                                                                pad_strip(my_row['Distributor']),
                                                                conv_string_date_sql(my_row['Release Date']),
                                                                pad_strip(my_row['Genre']),
                                                                pad_strip(my_row['MPAA Rating']),
                                                                my_row['Production Budget'],
                                                                conv_string_hour_sql(my_row['Runtime']),
                                                                remove_quotes(my_row['Plot']),
                                                                remove_quotes(my_row['Actors']),
                                                                my_row['imdbRating'],
                                                                my_row['imdbVotes'],
                                                                remove_quotes(my_row['Country']),
                                                                remove_quotes(my_row['Director']),
                                                                remove_quotes(my_row['Writer'])))
    connection.commit()
    connection.close()


def insert_into_daily_index(my_dataframe):
    """Function inserting values info the table daily index"""
    connection = pymysql.connect(user=config.username, password=config.pw, database= config.db)
    cur = connection.cursor()

    my_dataframe['Top 10 Gross'] = conv_dol_to_num(my_dataframe['Top 10 Gross'])
    my_dataframe['% Change YD'] = conv_per_to_num(my_dataframe['% Change YD'])
    my_dataframe['% Change LW'] = conv_per_to_num(my_dataframe['% Change LW'])
    my_dataframe['Gross'] = conv_dol_to_num(my_dataframe['Gross'])

    for my_index, my_row in my_dataframe.iterrows():
        cur.execute("""INSERT IGNORE INTO Daily_Index
                        (Daily_Date, Top_Movie, Day, Top_10_Gross, P_change_yd, P_change_lw, Movies_Tracked, Gross)
                        VALUES
                        ({}, {}, {}, {}, {}, {}, {}, {});""".format(conv_daily_date_sql(my_row['Date (clickto view)']),
                                                                    pad_strip(my_row['#1 Movie']),
                                                                    pad_strip(my_row['Day']),
                                                                    my_row['Top 10 Gross'],my_row['% Change YD'],
                                                                    my_row['% Change LW'],my_row['MoviesTracked'],
                                                                    my_row['Gross']))
    connection.commit()
    connection.close()


def check_dates_filtering(kwargs, year, soup3):
    """This function checks if the row we are going to add is in our range of dates if we have a filter on dates"""
    if to_datetime(year, DATE_DICT[soup3[TABLE_NUM].text.split()[ZERO_INDEX]],
                    soup3[TABLE_NUM].text.split()[TABLE_NUM]) <= kwargs['dates_filter'][
                TABLE_NUM] and to_datetime(year, DATE_DICT[soup3[TABLE_NUM].text.split()[ZERO_INDEX]],
                                           soup3[TABLE_NUM].text.split()[TABLE_NUM]) >= kwargs['dates_filter'][ZERO_INDEX]:
        return True


def add_extra_data_movies(liste_values, soup3, j):
    """Function appending interesting data about first ranked movies from the movie page itself"""
    liste_values.append(soup3[j].text)
    summary_site = SITE_DOMAIN + soup3[FILM_VALUE].a['href'].replace("daily", REPLACE_DAILY)
    my_soup2 = soup_maker(summary_site, 'html.parser')
    summary_table = my_soup2.find_all('table')[NEW_COLUMNS].find_all('td')
    for summary_element in range(FIRST_COL_INDEX, FILM_VALUE):
        liste_values.append(summary_table[summary_element].text.split(":")[-TABLE_NUM])
    return liste_values


def add_extra_columns(df_columns):
    """This function adds the extra columns we add in the main dataframe (with extra data on each movie)"""
    title_soup2 = soup_maker("https://www.boxofficemojo.com/movies/?id=wonderwoman.htm", 'html.parser')
    titles2 = title_soup2.find_all('table')[NEW_COLUMNS].find_all('td')
    for title_index in range(FIRST_COL_INDEX, FILM_VALUE):
        df_columns.append(titles2[title_index].text.split(":")[ZERO_INDEX])
    return df_columns


def add_extra_data_movies_imdb(liste_values, soup3):
    """Function appending interesting data about first ranked movies from the imdb website"""
    json = search_movie(soup3[FILM_VALUE].text)
    for value in IMDB_VALUES:
        try:
            if value == 'imdbRating':
                liste_values.append(float(json[value]))
            elif value == 'imdbVotes':
                liste_values.append(int(json[value].replace(',','')))
            else:
                liste_values.append(str(json[value]))
        except:
            if value == 'imdbRating':
                liste_values.append(0.0)
            elif value == 'imdbVotes':
                liste_values.append(0)
            else:
                liste_values.append('NA')

    return liste_values


def add_extra_columns_imdb(df_columns):
    """Function appending the columns we need because we added data from the imdb website"""
    for value in IMDB_VALUES:
        df_columns.append(value)
    return df_columns


def main():
    """Main function calling the config file and printing our resulting dataframe"""
    cm = config_manipulations(config.start_date, config.end_date, config.columns, config.args)
    if len(cm[FIRST_COL_INDEX]) != ZERO_INDEX :
        if len(cm[TABLE_NUM]) == ZERO_INDEX  :
            df = scraper(years=cm[ZERO_INDEX ], **cm[FIRST_COL_INDEX])
        elif len(cm[ZERO_INDEX ]) == ZERO_INDEX :
            df = scraper(df_columns=cm[TABLE_NUM],  **cm[FIRST_COL_INDEX])
        else:
            df = scraper(years=cm[ZERO_INDEX ], df_columns=cm[TABLE_NUM],**cm[FIRST_COL_INDEX])

    else:
        if len(cm[TABLE_NUM]) == ZERO_INDEX :
            df = scraper(years=cm[ZERO_INDEX ])
        elif len(cm[ZERO_INDEX ]) == ZERO_INDEX :
            df = scraper(df_columns=cm[TABLE_NUM])
        else:
            df = scraper(years=cm[ZERO_INDEX ], df_columns=cm[TABLE_NUM])
    insert_into_movie_values(df)
    insert_into_daily_index(df)
    print(df)


if __name__ == '__main__':
    main()