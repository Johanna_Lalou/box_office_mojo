import urllib
from urllib import error, parse, request
import json



KEY_IMDB = '866fe70d'
IMDB_URL = 'http://www.omdbapi.com/?'
APIKEY = '&apikey='+ KEY_IMDB


def search_movie(title):
    try:
        url = IMDB_URL + urllib.parse.urlencode({'t': title})+ APIKEY
        print('Retrieving the data of “{title}” now… ')
        uh = urllib.request.urlopen(url)
        data = uh.read()
        json_data = json.loads(data)
        return json_data
    except urllib.error.URLError as e:
        print("ERROR: {e.reason}".format(e))


print(search_movie('Avengers: Endgame'))
print(search_movie('Avengers: Endgame')['Actors'])
